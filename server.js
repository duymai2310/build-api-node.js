const port = 3000;
const express = require('express');
const app = express();
const path = require('path');   
const http = require('http');

app.use(express.json()); // work with type data json


// khai báo biến 
const courses =[
    {id :1 ,name : 'Node.js'},
    {id :2 ,name : 'Expressjs'},
    {id :3 ,name : 'HTML-CSS'},
    {id :4 ,name : 'JavaScript'},

];
const iotdev=" phát triển lorawan";


app.get('/',(req,res) => {
    res.send('chào mừng mai duy ');
    
})
app.get('/api/courses',(req,res) => {
    // console.log("chào mừng mai duy ");
    res.send(courses);
})
app.get('/about',(req,res) => {
    
    res.send(iotdev);
})

app.get('/api/courses/:id',(req,res) => {
    const course = courses.find(courses => courses.id === parseInt(req.params.id));
    if (!course) res.status(404).send("id not found"); 
    res.send(course);
        

});

// thêm phần tử 
app.post('/api/courses/add',(req,res) => {
    const course = {
        id : req.body.id,
        name : req.body.name,

    }
    courses.push(course);
    res.send(JSON.stringify({
        success : "true",
        notice  : "Add course successfully",
        data : courses
        
        }

    ));

});

app.put('/api/courses/edit/:id',(req,res) => {
    const course = courses.find(courses => courses.id === parseInt(req.params.id));
    // console.log(course);
    course.name = req.body.name;
    console.log(course);
    res.send(JSON.stringify({
        success : "true",
        notice  : `edit courses successfully`,
        data : courses
        
        }

    ));

});

// xóa phần tử 
app.delete('/api/courses/delete/:id',(req,res) => {
    const course = courses.find(courses => courses.id === parseInt(req.params.id));
    // console.log(course);
    let index = courses.indexOf(course);
    courses.splice(index, 1);
    res.send(JSON.stringify({
        success : "true",
        notice  : `delete courses successfully`,
        data : courses,
    
        
        }

    ));


});

app.listen(port,()=> console.log(`server listening on: http://localhost:${port}`));